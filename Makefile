
CXX = g++
FLAGS = -W -Wall -O2
LIBS = 
OBJS = main.o
TARGET = brainfuck

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(FLAGS) $(LIBS) $^ -o $@

%.o: %.cpp
	$(CXX) $(FLAGS) $(LIBS) -c $< -o $@
